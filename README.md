# Docker - Guía del Usuario

Bienvenidos a la guía para el usuario Docker.

Su intención es ser manual de referencia y guía de aprendizaje básico, es decir llevar rápidamente al uso de Docker sin demasiado énfasis en fundamentos o explicaciones de por qué suceden las cosas.

## Cómo utilizar esta guía

Esta guía está redactada en markdown y puede ser clonada a su disco para hacerle las modificaciones y mejoras que deseen.

La guía puede ser leída en forma secuencial o aleatorea, de acuerdo a las necesidades de cada uno

## A quién va dirigida esta guía

Inicialmente a cualquiera que esté interesado en Docker.

No es una guía básica, a pesar que empieza desde usos básicos de Docker, se recomienda entender los conceptos de contenedores en Linux y, obviamente, comprender los principios y objetivos de Docker.

Por eso, si no sabe lo que es Docker, se sugiere buscar material explicativo.

Si ya sabe lo que es Docker, esta guía lo acercará a su uso.

Por el contrario, si es un usuario de Docker, posiblemente encontrará poco útil esta guía, pero cada ficha tiene material adicional al que podrá consultar.

## Qué se puede aprender con esta guía

A usar docker.

Que docker es mucho más que esta guía, y que vale la pena cada minuto invertido en aprenderlo.

Que docker es muy divertido.

# Usar esta guía localmente

Si tienes docker instalado y funcionando, ejecuta estos pasos para tener la guía local y modificarla como quieras:

```
git clone https://gitlab.com/pilasguru/Docker-GuiaParaElUsuario.git

cd Docker-GuiaParaElUsuario/

docker build -t dgu .

docker run --rm -p 8000:8000 dgu
```

y ya puedes abrir el navegador en `http://localhost:8000/` para usar la guía.  Y en cuanto des Ctrl-C o cierres la ventana donde has ejecutado el `docker run` previo, todo se cerrara.

Comentario: el sistema de documentación [MkDocs](https://www.mkdocs.org/) ofrece integrado un sistema de desarrollo más optimo que estar haciendo `docker build` de una imagen docker para ver los cambios realizados.

:wq

